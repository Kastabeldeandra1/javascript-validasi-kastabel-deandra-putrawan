document.getElementById('loginForm').addEventListener('submit', function(event) {
    event.preventDefault(); // Mencegah form untuk di submit

    // Ambil nilai dari input
    var username = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('pass').value;
    var confirmPassword = document.getElementById('conf').value;

    // Validasi input kosong
    if (username === '' || email === '' || password === '' || confirmPassword === '') {
        alert('Semua kolom harus diisi!');
        return;
    }

    // Validasi email
    if (!validateEmail(email)) {
        alert('Email tidak valid!');
        return;
    }

    // Validasi panjang password
    if (password.length < 8) {
        alert('Password minimal 8 karakter!');
        return;
    }

    // Validasi konfirmasi password
    if (password !== confirmPassword) {
        alert('Password tidak cocok!');
        return;
    }

    // Jika semua validasi lolos
    alert('Login berhasil!\nUsername: ' + username + '\nEmail: ' + email);
});

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(String(email).toLowerCase());
}